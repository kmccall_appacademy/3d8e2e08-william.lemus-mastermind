class Code
  attr_reader :pegs
  PEGS = {}
  COLORS = 'rgbyop'
  def initialize(pegs)
    @pegs = pegs
  end

  def self.parse(str)
    colors = 'rgbyop'
    raise "Invalid input" unless str.chars.all? { |ch| COLORS.include?(ch.downcase) }
    Code.new(str.chars)
  end

  def self.random
    nums = []
    4.times do
      nums << rand(0..5)
    end
    str = ''
    nums.each { |num| str << COLORS[num] }
    Code.new(str)
  end

  def [](idx)
    @pegs[idx] if idx.class == Fixnum
  end

  def []=(arr)
    arr.join
  end

  def exact_matches(other)
    arr = other.pegs
    num_matches = 0
    arr.each_with_index do |ch, idx|
      num_matches += 1 if @pegs[idx].casecmp(ch).zero?
    end
    num_matches
  end

  def near_matches(other)
    arr = other.pegs
    num_matches = 0
    temp_pegs = @pegs.dup
    arr.each_with_index do |ch, idx|
      if temp_pegs.include?(ch.downcase)
        num_matches += 1
        temp_pegs[temp_pegs.index(ch.downcase)] = ''
      end
    end
    num_matches -= exact_matches(other) if num_matches > 0
    num_matches
  end

  def ==(other)
    return true if other.class == Code && other.pegs.join.casecmp(@pegs.join).zero?
    false
  end

end

class Game
  attr_reader :secret_code
  def initialize(code = Code.random)
    @secret_code = code
  end

  def get_guess
    puts "Enter a guess:"
    guess = $stdin.gets.chomp
    Code.parse(guess)
  end

  def display_matches(guess)
    puts "exact matches: #{secret_code.exact_matches(guess)}"
    puts "near matches: #{secret_code.near_matches(guess)}"
  end

  def play
    won = false
    10.times do |num|
      guess = get_guess
      if @secret_code.exact_matches(guess) == 4
        puts "you guessed it!"
        won = true
        break
      end
      puts "Not an exact code match!"
      display_matches(guess)
      puts "#{9-num} guessed left\n\n" if num < 9
    end
    puts "No more guesses! Ahh try again next time!\n\n" if won == false
  end

end

new_game = Game.new(Code.parse("BBBB"))
new_game.play
